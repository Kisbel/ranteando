
const express = require("express");
const app = express();
const port = 3000;
const session = require('express-session');
const mysql = require('mysql');
const connection = mysql.createConnection ({
    host : 'localhost',
    user: 'kisbel',
    password: 'kisbelpass',
    database: 'quejas',
});
const MySQLStore = require("express-mysql-session")(session);

const bcrypt = require('bcrypt'); //BCRYPT
const saltRounds = 10;

const { doubleCsrf } = require("csrf-csrf"); // CSRF
const cookieParser = require('cookie-parser')

const multer = require('multer');
const upload = multer({ dest: 'upload' }); 

//nunjucks
const nunjucks = require("nunjucks");
app.engine ("njk", nunjucks.render);
app.set("view engine", "njk");
nunjucks.configure("views", {
  autoescape: true,
  express: app
});


//funcion para mensajes flash (q al refrescar se eliminen)
function flash (req, res, next) {
  if (req.session && req.session.flash) {
  req.locals = {};
  req.locals.flash = req.session.flash;
  delete req.session.flash;
  }
  next();
  };


//usar el middleware de flash
app.use(flash);

// mete los datos del formulario en el req.body
app.use(express.urlencoded()); 


// puerto
app.listen(port, () => {
  console.log("App listening on port 3000!")
});


// link /style.css 
app.use(express.static(__dirname + '/css'));
app.use('/css', express.static(__dirname + '/css'));

// Serve uploads as static files
app.use("/upload", express.static('upload'));

//MYSQL
app.use(session({
  key: 'uwu',
  secret: 'uwu', 
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false },
  store: sessionStore,
}));

//MYSQL NEW DATABASE

connection.connect(function(err){
    if (err) {
        console.error('Connection error'); 
        return;
    }
    console.log ('MYSQL Connected succesfully!'); 
});

var options ={
    host: 'localhost',
    port: 3306,
    user: 'kissss',
    password: 'kissss',
    database: 'quejas'
};

var sessionConnection = mysql.createConnection(options);

var sessionStore = new MySQLStore({
    expiration: 10800000,
    createDatabaseTable: true,
    schema:{
        tableName: 'sessionskisbel',
        columnNames:{
            session_id: 'sesssion_id',
            expires: 'expires',
            data: 'data'
        }
    }
},sessionConnection);

/////CSRF
//usar el middleware de cookieParser
app.use(cookieParser("this is a secret"));

const csrf_config= {
  getSecret: () => "Secret",
  cookieName: "__Host-psifi.x-csrf-token",
  cookieOptions: {
    httpOnly: true,
    sameSite: "lax",
    path: "/",
  },
  size: 64, 
  ignoredMethods: ["GET", "HEAD", "OPTIONS"],
  getTokenFromRequest: (req) => req.body.csrf_token,
};

const {
  invalidCsrfTokenError,
  generateToken,
  validateRequest,
  doubleCsrfProtection,
} = doubleCsrf(csrf_config);



/////////////////////////////////////////////////////////////////////7
// Estas 6 renderizan las paginas /


app.get('/', function(req, res) {
  res.render("start.html");
});

const gq = require("./database.js") 


app.get('/dashboard', flash, async (req, res) => {
  if (req.session.loggedin) {
    let quejas = await gq.getQuejas();
    let flashy= req.locals ? req.locals.flash : null;
    res.render("index.html", {quejas: quejas.result, usuario: req.session.usuario, flashy: flashy});
  } else {
    res.render('inicie.html');
  }
});


/*app.get('/dashboard', function(req, res) {
  if (req.session.loggedin) { //Si ha iniciado sesion
    connection.query('SELECT * FROM quejas', (err, rows, fields) => {
    if (err) throw err;
     res.render("index.html", {quejas: rows, usuario: req.session.usuario});})
  } else {// si no ha iniciado sesion
  res.send('Inicia sesión');}
});
*/


/*app.get('/dashboard', function(req, res) {
  if (req.session.loggedin) { //Si ha iniciado sesion
    connection.query('SELECT * FROM usuarios WHERE username = ?', username, function (err, rows, fields)=>{
      if (err) throw err;
      connection.query('SELECT * FROM quejas', (err, rows, fields) => {
      if (err) throw err;
      res.render("index.html", {quejas: rows, usuario: rows});})});
  } else {// si no ha iniciado sesion
  res.send('Inicia sesión');}
});*/


app.get("/form", function (req,res){
  if (req.session.loggedin) {
  const token = generateToken(res, req); 
  res.render("form.html", {usuario: req.session.usuario, csrf_token: token,});} else 
  {res.render('inicie.html');}
});

app.get("/basededatos", function (req,res){
  res.render("basededatos.html");
});

app.get("/quejascheck", function (req,res){
  res.render("quejascheck.html");
});

app.get('/login', (req, res)=>{
  const token = generateToken(res, req); // Ojo al orden de los argumentos
  res.render("login.html", {csrf_token: token});
});


app.get('/registro', (req, res)=>{
  const token = generateToken(res, req); 
  res.render("registro.html", {csrf_token: token});
});

app.get('/cambio', async (req, res)=>{
  const token = generateToken(res, req); 
  res.render("cambio.html", {csrf_token: token});
});

app.get('/queja', function(req, res) {
  connection.query('SELECT * FROM quejas', (err, rows, fields) => {
    if (err) throw err;
  res.render("queja.html", {quejas: rows}); //SELECCIONA DE LA BASE DE DATOS
});
});

app.get ('/queja/:id', async (req, res) => {
  if (req.session.loggedin){
    let quejita = await gq.quejasID(req, res);
    res.render('quejacheck.html', {quejita: quejita.result});} else {
    res.render('inicie.html');}
});

app.get ('/perfil', async (req, res) => {
  if (req.session.loggedin){
    let fotito = await gq.profilePic(req, res);
    res.render('perfil.html', {fotito: fotito.result, usuario: req.session.usuario});} else{
    res.render('inicie.html');}
});


//////////////////// SESIONES


///////////////////////////////////

app.post('/login', doubleCsrfProtection, function(req, res) {
  const usuario = req.body.username;
  const contraseña = req.body.password;
    // El query conulta la base de datos 'usuarios' para encontrar el username en columnas username
    connection.query("SELECT * FROM usuarios WHERE ?", {username: usuario}, (err, result)=>{
      if(err) throw err;
      //Si la consulta retorna un usuario con longitud de 1, compara contraseñas abajo
      if (result.length !==1){
        res.send("Not a user");
        return;}
        bcrypt.compare(contraseña, result[0].password, function(err, pass_correct) {
          if (err) throw err;
          if (pass_correct){
          req.session.regenerate(function (err) {
          if (err) {throw err};
      // guarda información del usuario en la sesion
           //logeado es true
            req.session.loggedin = true;
            req.session.usuario= result[0].username;
            console.log("Hola ", req.session.usuario);
            req.session.userId = result[0].id;
            req.session.flash = "Bienvenido";       
      // guarda la sesion antes de redireccionar
            req.session.save(function (err) {
              if (err) throw err;
            res.redirect('/dashboard')
            })
          });
      } else {
        console.log("wrong pass")
        res.send("Wrong pass");}
    });
});});



let avatarUpload = upload.single("avatar");
// El middleware de los uploads (multer) se encarga de procesar los formularios
// con enctype="multipart/form-data" y meter los valores en el `req.body`.

app.post('/registro', avatarUpload, doubleCsrfProtection, async (req, res)=>{
  const usuario = req.body.username;
  const contraseña = req.body.password;
  const avatar = req.file ? req.file.path : "";
  let hash = await bcrypt.hash(contraseña, saltRounds);
  const userNew = {username: usuario, password: hash, avatar: avatar};
    try {
    await gq.insertUser(userNew);
    } catch (err) {
    if (err.code === "ER_DUP_ENTRY") {
      res.send("El usuario ya existe");
      return;
    }
    throw err;
  }
  res.render("registrocheck.html");
});

app.post('/cambio', avatarUpload, doubleCsrfProtection, async (req, res)=>{
  const usuario = req.body.username;
  const contraseña = req.body.password;
  const avatar = req.file ? req.file.path : "";
  let hash = await bcrypt.hash(contraseña, saltRounds);
  const userNew = {username: usuario, password: hash, avatar: avatar};
    try {
    await gq.insertUser(userNew);
    } catch (err) {
    if (err.code === "ER_DUP_ENTRY") {
      res.send("El usuario ya existe");
      return;
    }
    throw err;
  }
  res.send("perfil.html",);
});



//postear lo que te dan en el formulario en /queja y LUEGO el index.html lo coje y lo pone en su lista

app.post ('/queja', doubleCsrfProtection, (req, res)=> {
  const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    let datos = {
        body: req.body.body,
        name: req.session.userId,
        date: new Date().toISOString().slice(0, 19).replace('T', ' '),
    };
    connection.query ('INSERT INTO quejas SET ?', datos, function (err, rows, fields) { 
        if (err) {
            throw err;
        }
        console.log ('Values added succesfully!');
        res.render("queja.html", {datos, usuario: req.session.usuario}); 
    });
}); 


app.get('/logout', (req, res)=>{
  req.session.userId = null;
  req.session.save(function (err) {
    if (err) throw err;
    // Regenerate the session, which is good practice to help
    // guard against forms of session fixation
    // Check express-session's documentation to learn more about this
    req.session.regenerate(function (err) {
      if (err) throw err;
      res.redirect('/')
    })
  })
});



/*TODO:
-mysql express session
-migraciones
*/