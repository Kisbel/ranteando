const mysql = require('mysql');
const connection = mysql.createConnection ({
  host : 'localhost',
  user: 'kisbel',
  password: 'kisbelpass',
  database: 'quejas',
});
connection.connect();

////////////////////////////////////////////////////


function getQuejas (){
  return new Promise ((resolve, reject) =>
  connection.query("SELECT quejas.id, body, name, username, date, avatar FROM quejas LEFT JOIN usuarios ON quejas.name = usuarios.id", (err, rows, fields)=>{
    if (err) reject (err);
    resolve({result: rows, fields: fields})
  }))
};

function quejasID(req, res) {
  const id = req.params.id;
  return new Promise((resolve, reject) => {
    connection.query(
      'SELECT * FROM quejas LEFT JOIN usuarios ON quejas.name = usuarios.id WHERE quejas.id = ?', id, (err, rows) => {
        if (err) reject(err);
        console.log(rows);
        resolve({ result: rows[0] });
      }
    );
  });
}


function insertUser(usuario){
  console.log (usuario);
  return new Promise((resolve, reject)=>{
    connection.query('INSERT INTO usuarios SET ?',
      {username: usuario.username, password: usuario.password, avatar: usuario.avatar},
      (err, result)=>{
        if(err) reject(err);
        resolve();
      });
  });
}


function profilePic(req, res) {
  return new Promise((resolve, reject) => {
    connection.query('SELECT username FROM usuarios ', (err, rows) => {
        if (err) reject(err);
        console.log(rows[0]);
        resolve({ result: rows[0] });
      }
    );
  });
}

function profilePic(req, res) {
  const userId = req.session.userId;
  return new Promise((resolve, reject) => {
    connection.query('SELECT username, avatar FROM usuarios WHERE id = ?', userId, (err, rows) => {
        if (err) reject(err);
        console.log(rows[0]);
        resolve({ result: rows[0] });
      }
    );
  });
}

module.exports = { getQuejas, quejasID, insertUser, profilePic};

